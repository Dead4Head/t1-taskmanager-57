package ru.t1.amsmirnov.taskmanager.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class DataXmlSaveJaxBRequest extends AbstractUserRequest {

    public DataXmlSaveJaxBRequest() {
    }

    public DataXmlSaveJaxBRequest(@Nullable String token) {
        super(token);
    }

}
