package ru.t1.amsmirnov.taskmanager.dto.request.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class TaskClearRequest extends AbstractUserRequest {

    public TaskClearRequest() {
    }

    public TaskClearRequest(@Nullable final String token) {
        super(token);
    }

}