package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class TaskUnbindFromProjectResponse extends AbstractResultResponse {

    public TaskUnbindFromProjectResponse() {
    }

    public TaskUnbindFromProjectResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}