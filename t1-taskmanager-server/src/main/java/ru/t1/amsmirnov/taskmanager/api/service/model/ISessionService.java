package ru.t1.amsmirnov.taskmanager.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
