package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.amsmirnov.taskmanager.api.repository.model.IAbstractUserOwnedRepository;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;


@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M>
        implements IAbstractUserOwnedRepository<M> {


    public AbstractUserOwnedRepository() {
        super();
    }

}
