package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.amsmirnov.taskmanager.api.repository.model.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.model.Project;

import java.util.List;

@Repository
@Scope("prototype")
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository() {
        super();
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        final String jql = "SELECT p FROM Project p ORDER BY p.created";
        return entityManager.createQuery(jql, Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllSorted(@Nullable final String sort) {
        if (sort == null || sort.isEmpty()) return findAll();
        final String jql = "SELECT p FROM Project p ORDER BY :sort";
        return entityManager.createQuery(jql, Project.class)
                .setParameter("sort", sort)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        final String jql = "SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY p.created";
        return entityManager.createQuery(jql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllSorted(
            @NotNull final String userId,
            @Nullable final String sort
    ) {
        if (sort == null || sort.isEmpty()) return findAll(userId);
        final String jql = "SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY :sort";
        return entityManager.createQuery(jql, Project.class)
                .setParameter("userId", userId)
                .setParameter("sort", sort)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String id) {
        final String jql = "SELECT p FROM Project p WHERE p.id = :id";
        return entityManager.createQuery(jql, Project.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Project findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String jql = "SELECT p FROM Project p WHERE p.user.id = :userId AND p.id = :id";
        return entityManager.createQuery(jql, Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

    }

    @Override
    public void removeAll() {
        final String jql = "DELETE FROM Project";
        entityManager.createQuery(jql).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull String userId) {
        final String jql = "DELETE FROM Project p WHERE p.user.id = :userId";
        entityManager.createQuery(jql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
