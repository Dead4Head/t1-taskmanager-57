package ru.t1.amsmirnov.taskmanager.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.amsmirnov.taskmanager.api.service.IDBProperty;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Session;
import ru.t1.amsmirnov.taskmanager.model.Task;
import ru.t1.amsmirnov.taskmanager.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

import static org.hibernate.cfg.AvailableSettings.*;

@Configuration
@ComponentScan("ru.t1.amsmirnov.taskmanager")
public class ServerConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(@NotNull final IDBProperty dataBaseProperty) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, dataBaseProperty.getDatabaseDriver());
        settings.put(Environment.URL, dataBaseProperty.getDatabaseUrl());

        settings.put(Environment.USER, dataBaseProperty.getDatabaseUser());
        settings.put(Environment.PASS, dataBaseProperty.getDatabasePassword());
        settings.put(Environment.DIALECT, dataBaseProperty.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, dataBaseProperty.getDatabaseHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, dataBaseProperty.getDatabaseShowSql());
        settings.put(Environment.FORMAT_SQL, dataBaseProperty.getDatabaseFormatSql());
        settings.put(Environment.USE_SQL_COMMENTS, dataBaseProperty.getDatabaseCommentsSql());

        settings.put(USE_SECOND_LEVEL_CACHE, dataBaseProperty.getDataBaseSecondLvlCache());
        settings.put(CACHE_REGION_FACTORY, dataBaseProperty.getDataBaseFactoryClass());
        settings.put(USE_QUERY_CACHE, dataBaseProperty.getDataBaseUseQueryCache());
        settings.put(USE_MINIMAL_PUTS, dataBaseProperty.getDataBaseUseMinPuts());
        settings.put(CACHE_REGION_PREFIX, dataBaseProperty.getDataBaseRegionPrefix());
        settings.put(CACHE_PROVIDER_CONFIG, dataBaseProperty.getDataBaseHazelConfig());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}
