package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IProjectDtoRepository;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;

import java.util.List;

@Repository
@Scope("prototype")
public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    public ProjectDtoRepository() {
        super();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        final String jql = "SELECT p FROM ProjectDTO p ORDER BY p.created";
        return entityManager.createQuery(jql, ProjectDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSorted(@Nullable final String sort) {
        if (sort == null || sort.isEmpty()) return findAll();
        final String jql = "SELECT p FROM ProjectDTO p ORDER BY p." + sort;
        return entityManager.createQuery(jql, ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        final String jql = "SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.created";
        return entityManager.createQuery(jql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSorted(
            @NotNull final String userId,
            @Nullable final String sort
    ) {
        if (sort == null || sort.isEmpty()) return findAll(userId);
        final String jql = "SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p." + sort;
        return entityManager.createQuery(jql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String id) {
        final String jql = "SELECT p FROM ProjectDTO p WHERE p.id = :id";
        return entityManager.createQuery(jql, ProjectDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        final String jql = "SELECT p FROM ProjectDTO p WHERE p.userId = :userId AND p.id = :id";
        return entityManager.createQuery(jql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

    }

    @Override
    public void removeAll() {
        @NotNull final String jql = "DELETE FROM ProjectDTO";
        entityManager.createQuery(jql).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final String jql = "DELETE FROM ProjectDTO p WHERE p.userId = :userId";
        entityManager.createQuery(jql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
