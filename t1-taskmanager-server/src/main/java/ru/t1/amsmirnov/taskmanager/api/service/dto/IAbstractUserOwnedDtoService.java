package ru.t1.amsmirnov.taskmanager.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

import java.util.Comparator;
import java.util.List;

public interface IAbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO> extends IAbstractDtoService<M> {

    @NotNull
    M add(@Nullable String userId, @Nullable M model) throws AbstractException;

    @NotNull
    List<M> findAll(@Nullable String userId) throws AbstractException;

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator) throws AbstractException;

    @NotNull
    M findOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    M removeOne(@Nullable String userId, @Nullable M model) throws AbstractException;

    @NotNull
    M removeOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    void removeAll(@Nullable String userId) throws AbstractException;

    boolean existById(@Nullable String userId, @Nullable String id) throws AbstractException;

    int getSize(@Nullable String userId) throws AbstractException;

}
