package ru.t1.amsmirnov.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.api.repository.model.IUserRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
import ru.t1.amsmirnov.taskmanager.api.service.model.IUserService;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.EmailEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.LoginEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.PasswordEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.user.EmailExistException;
import ru.t1.amsmirnov.taskmanager.exception.user.LoginExistException;
import ru.t1.amsmirnov.taskmanager.model.User;
import ru.t1.amsmirnov.taskmanager.util.HashUtil;

import javax.persistence.EntityManager;

@Service
public final class UserService
        extends AbstractModelService<User, IUserRepository>
        implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    public UserService() {
        super();
    }

    @NotNull
    @Override
    protected IUserRepository getRepository() {
        return context.getBean(IUserRepository.class);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmailExist(email)) throw new EmailExistException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        if (role != null) user.setRole(role);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        return create(login, password, email, null);
    }


    @NotNull
    @Override
    public User findOneByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final User user = repository.findOneByLogin(login);
            if (user == null) throw new UserNotFoundException();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User findOneByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final User user = repository.findOneByEmail(email);
            if (user == null) throw new UserNotFoundException();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try {
            findOneByLogin(login);
            return true;
        } catch (final AbstractException e) {
            return false;
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try {
            findOneByEmail(email);
            return true;
        } catch (final AbstractException e) {
            return false;
        }
    }

    @NotNull
    @Override
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findOneById(id);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        return update(user);
    }

    @NotNull
    @Override
    public User updateUserById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractException {
        @NotNull User user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return update(user);
    }

    @Override
    public void lockUserByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findOneByLogin(login);
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findOneByLogin(login);
        user.setLocked(false);
        update(user);
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) throws AbstractException {
        @NotNull final User user = findOneByLogin(login);
        return removeOne(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) throws AbstractException {
        @NotNull final User user = findOneByEmail(email);
        return removeOne(user);
    }

}
