//package ru.t1.amsmirnov.taskmanager.repository;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.*;
//import org.junit.experimental.categories.Category;
//import ru.t1.amsmirnov.taskmanager.api.repository.dto.IProjectDtoRepository;
//import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;
//import ru.t1.amsmirnov.taskmanager.enumerated.ProjectSort;
//import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
//import ru.t1.amsmirnov.taskmanager.repository.dto.ProjectDtoRepository;
//import ru.t1.amsmirnov.taskmanager.repository.dto.UserDtoRepository;
//
//import java.util.ArrayList;
//import java.util.Comparator;
//import java.util.List;
//import java.util.stream.Collectors;
//
//import static org.junit.Assert.*;
//
//@Category(DBCategory.class)
//public class ProjectRepositoryTest extends AbstractUserOwnedRepositoryTest {
//
//    @NotNull
//    private IProjectDtoRepository projectRepository;
//
//    @NotNull
//    private List<ProjectDTO> projects;
//
//    @NotNull
//    private List<ProjectDTO> alfaProjects;
//
//    @NotNull
//    private List<ProjectDTO> betaProjects;
//
//    @Before
//    public void initRepository() {
//        projectRepository = new ProjectDtoRepository(ENTITY_MANAGER);
//        projects = new ArrayList<>();
//        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
//            @NotNull final ProjectDTO project = new ProjectDTO();
//            project.setName("ProjectDTO " + (NUMBER_OF_ENTRIES - i));
//            project.setDescription("Description " + i);
//            if (i < 5)
//                project.setUserId(USER_ALFA_ID);
//            else
//                project.setUserId(USER_BETA_ID);
//            try {
//                TRANSACTION.begin();
//                projectRepository.add(project);
//                projects.add(project);
//                TRANSACTION.commit();
//            } catch (final Exception e) {
//                TRANSACTION.rollback();
//                throw e;
//            }
//        }
//        projects = projects.stream()
//                .sorted(ProjectSort.BY_CREATED.getComparator())
//                .collect(Collectors.toList());
//        alfaProjects = projects
//                .stream()
//                .filter(p -> USER_ALFA_ID.equals(p.getUserId()))
//                .collect(Collectors.toList());
//        betaProjects = projects
//                .stream()
//                .filter(p -> USER_BETA_ID.equals(p.getUserId()))
//                .collect(Collectors.toList());
//    }
//
//    @After
//    public void clearRepository() {
//        try {
//            TRANSACTION.begin();
//            projectRepository.removeAll();
//            TRANSACTION.commit();
//        } catch (final Exception e) {
//            TRANSACTION.rollback();
//            throw e;
//        }
//        projects.clear();
//    }
//
//    @Test
//    public void testAdd() {
//        @NotNull final ProjectDTO project = new ProjectDTO();
//        project.setName("Added project");
//        project.setDescription("Added description");
//        project.setUserId(USER_ALFA_ID);
//        try {
//            TRANSACTION.begin();
//            projectRepository.add(project);
//            TRANSACTION.commit();
//            projects.add(project);
//            @Nullable final List<ProjectDTO> actualProjectList = projectRepository.findAll();
//            projects.sort(ProjectSort.BY_CREATED.getComparator());
//            assertEquals(projects, actualProjectList);
//        } catch (final Exception e) {
//            TRANSACTION.rollback();
//            ENTITY_MANAGER.close();
//            throw e;
//        }
//    }
//
//    @Test
//    public void testRemoveAll() {
//        try {
//            assertNotEquals(0, projectRepository.findAll().size());
//            TRANSACTION.begin();
//            projectRepository.removeAll();
//            TRANSACTION.commit();
//            assertEquals(0, projectRepository.findAll().size());
//        } catch (final Exception e) {
//            TRANSACTION.rollback();
//            throw e;
//        }
//    }
//
//    @Test
//    public void testRemoveAllForUser() {
//        try {
//            assertNotEquals(0, projectRepository.findAll(USER_ALFA_ID).size());
//            TRANSACTION.begin();
//            projectRepository.removeAll(USER_ALFA_ID);
//            TRANSACTION.commit();
//            assertEquals(0, projectRepository.findAll(USER_ALFA_ID).size());
//        } catch (final Exception e) {
//            TRANSACTION.rollback();
//            throw e;
//        }
//    }
//
//    @Test
//    public void testFindAll() {
//        List<ProjectDTO> actualAlfaProjects = projectRepository.findAll(USER_ALFA_ID);
//        List<ProjectDTO> actualBetaProjects = projectRepository.findAll(USER_BETA_ID);
//        assertNotEquals(actualAlfaProjects, actualBetaProjects);
//        assertEquals(alfaProjects, actualAlfaProjects);
//        assertEquals(betaProjects, actualBetaProjects);
//    }
//
//    @Test
//    public void testFindAllComparator() {
//        @NotNull final Comparator<ProjectDTO> comparator = ProjectSort.BY_NAME.getComparator();
//        List<ProjectDTO> actualAlfaProjects = projectRepository.findAllSorted(USER_ALFA_ID, "name");
//        alfaProjects = alfaProjects.stream().sorted(comparator).collect(Collectors.toList());
//        assertEquals(alfaProjects, actualAlfaProjects);
//    }
//
//    @Test
//    public void testFindOneById() {
//        for (final ProjectDTO project : projects) {
//            assertEquals(project, projectRepository.findOneById(project.getUserId(), project.getId()));
//        }
//        assertNull(projectRepository.findOneById(USER_ALFA_ID, NONE_ID));
//    }
//
//}
