//package ru.t1.amsmirnov.taskmanager.repository;
//
//import org.jetbrains.annotations.NotNull;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.experimental.categories.Category;
//import ru.t1.amsmirnov.taskmanager.api.repository.dto.ISessionDtoRepository;
//import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;
//import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
//import ru.t1.amsmirnov.taskmanager.repository.dto.SessionDtoRepository;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//@Category(DBCategory.class)
//public class SessionRepositoryTest extends AbstractUserOwnedRepositoryTest {
//
//    @NotNull
//    private static final Date TODAY = new Date();
//
//    @NotNull
//    private final List<SessionDTO> sessions = new ArrayList<>();
//
//    private ISessionDtoRepository sessionRepository;
//
//    @Before
//    public void initRepository() throws Exception {
//        ENTITY_MANAGER = CONNECTION_SERVICE.getEntityManager();
//        TRANSACTION = ENTITY_MANAGER.getTransaction();
//        sessionRepository = new SessionDtoRepository(ENTITY_MANAGER);
//        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
//            Thread.sleep(2);
//            @NotNull final SessionDTO session = new SessionDTO();
//            session.setCreated(TODAY);
//            if (i <= 5)
//                session.setUserId(USER_ALFA_ID);
//            else
//                session.setUserId(USER_BETA_ID);
//            try {
//                sessions.add(session);
//                sessionRepository.add(session);
//            } catch (final Exception e) {
//                TRANSACTION.rollback();
//                throw e;
//            }
//        }
//    }
//
//    @After
//    public void clearRepository() throws Exception {
//        try {
//            sessionRepository.removeAll();
//            sessions.clear();
//        } catch (final Exception e) {
//            TRANSACTION.rollback();
//            throw e;
//        } finally {
//            ENTITY_MANAGER.close();
//        }
//    }
//
//}
