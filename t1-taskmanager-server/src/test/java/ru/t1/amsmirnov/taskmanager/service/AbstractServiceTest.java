//package ru.t1.amsmirnov.taskmanager.service;
//
//import liquibase.Liquibase;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.BeforeClass;
//import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
//import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
//import ru.t1.amsmirnov.taskmanager.migration.AbstractSchemeTest;
//
//public abstract class AbstractServiceTest extends AbstractSchemeTest {
//
//    @NotNull
//    protected static final IPropertyService PROPERTY_SERVICE = new PropertyService();
//
//    protected static final int NUMBER_OF_ENTRIES = 10;
//
//    @NotNull
//    protected static final String NONE_STR = "---NONE---";
//
//    @Nullable
//    protected static final String NULL_STR = null;
//
//    @BeforeClass
//    public static void initDataBase() throws Exception {
//        final Liquibase liquibase = CONNECTION_SERVICE.getLiquibase();
//        liquibase.dropAll();
//        liquibase.update("scheme");
//    }
//
//}
