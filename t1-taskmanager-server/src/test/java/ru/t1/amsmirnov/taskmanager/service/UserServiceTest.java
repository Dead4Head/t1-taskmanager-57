//package ru.t1.amsmirnov.taskmanager.service;
//
//import org.jetbrains.annotations.NotNull;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//import ru.t1.amsmirnov.taskmanager.api.service.dto.IProjectDtoService;
//import ru.t1.amsmirnov.taskmanager.api.service.dto.ITaskDtoService;
//import ru.t1.amsmirnov.taskmanager.api.service.dto.IUserDtoService;
//import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;
//import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
//import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
//import ru.t1.amsmirnov.taskmanager.enumerated.Role;
//import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
//import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
//import ru.t1.amsmirnov.taskmanager.exception.entity.UserNotFoundException;
//import ru.t1.amsmirnov.taskmanager.exception.field.EmailEmptyException;
//import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
//import ru.t1.amsmirnov.taskmanager.exception.field.LoginEmptyException;
//import ru.t1.amsmirnov.taskmanager.exception.field.PasswordEmptyException;
//import ru.t1.amsmirnov.taskmanager.exception.user.EmailExistException;
//import ru.t1.amsmirnov.taskmanager.exception.user.LoginExistException;
//import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
//import ru.t1.amsmirnov.taskmanager.service.dto.ProjectDtoService;
//import ru.t1.amsmirnov.taskmanager.service.dto.TaskDtoService;
//import ru.t1.amsmirnov.taskmanager.service.dto.UserDtoService;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.junit.Assert.*;
//
//@Category(DBCategory.class)
//public class UserServiceTest extends AbstractServiceTest {
//
//
//    private final IProjectDtoService projectService = new ProjectDtoService(CONNECTION_SERVICE);
//
//    @NotNull
//    private final ITaskDtoService taskService = new TaskDtoService(CONNECTION_SERVICE);
//
//    @NotNull
//    private final IUserDtoService userService = new UserDtoService(CONNECTION_SERVICE, PROPERTY_SERVICE);
//
//    @NotNull
//    private final List<UserDTO> users = new ArrayList<>();
//
//    @Before
//    public void initRepository() throws Exception {
//        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
//            Thread.sleep(1);
//            @NotNull final UserDTO user = new UserDTO();
//            user.setFirstName("UserFirstName " + i);
//            user.setLastName("UserLastName " + i);
//            user.setMiddleName("UserMidName " + i);
//            user.setEmail("user" + i + "@dot.ru");
//            user.setLogin("USER" + (NUMBER_OF_ENTRIES - i));
//            user.setPasswordHash(user.getId());
//            user.setRole(Role.USUAL);
//            userService.add(user);
//            users.add(user);
//
//            for (int j = 0; j < NUMBER_OF_ENTRIES / 4; j++) {
//                Thread.sleep(1);
//                @NotNull final ProjectDTO project = new ProjectDTO();
//                project.setName("ProjectDTO name: " + i + " " + j);
//                project.setDescription("ProjectDTO description: " + i + " " + j);
//                project.setUserId(user.getId());
//                projectService.add(project);
//
//                for (int a = 0; a < NUMBER_OF_ENTRIES / 4; a++) {
//                    Thread.sleep(1);
//                    @NotNull final TaskDTO task = new TaskDTO();
//                    task.setName("TaskDTO name: " + a);
//                    task.setDescription("TaskDTO description: " + a);
//                    task.setUserId(user.getId());
//                    task.setProjectId(project.getId());
//                    taskService.add(task);
//                }
//            }
//        }
//    }
//
//    @After
//    public void clearRepository() {
//        taskService.removeAll();
//        projectService.removeAll();
//        userService.removeAll();
//    }
//
//    @Test
//    public void testCreate() throws AbstractException {
//        List<UserDTO> usersRepo = userService.findAll();
//        assertEquals(users, usersRepo);
//
//        UserDTO user = userService.create("TEST_USER1", "TEST_PASSWORD", "testuser1@t.com");
//        users.add(user);
//        users.add(userService.create("TEST_USER2", "TEST_PASSWORD", "user@mail.com"));
//        users.add(userService.create("TEST_ADMIN", "TESTADMIN", "TEST_ADMIN@e.com", Role.ADMIN));
//        usersRepo = userService.findAll();
//        assertEquals(users.size(), usersRepo.size());
//    }
//
//    @Test(expected = EmailExistException.class)
//    public void testCreate_EmailExistException_1() throws AbstractException {
//        userService.create("LOGIN1", "TEST_PASSWORD", "test@test.test");
//        userService.create("LOGIN2", "TEST_PASSWORD", "test@test.test");
//    }
//
//    @Test(expected = LoginEmptyException.class)
//    public void testCreate_EmptyLoginException_1() throws AbstractException {
//        userService.create("", "TEST_PASSWORD", "newtest@t.com");
//    }
//
//    @Test(expected = LoginEmptyException.class)
//    public void testCreate_EmptyLoginException_2() throws AbstractException {
//        userService.create(NULL_STR, "TEST_PASSWORD", "newtest@t.com");
//    }
//
//    @Test(expected = LoginExistException.class)
//    public void testCreate_LoginExistException() throws AbstractException {
//        userService.create(NONE_STR, "TEST_PASSWORD", "newtest@t.com");
//        userService.create(NONE_STR, "TEST_PASSWORD", "newtest@t.com");
//    }
//
//    @Test(expected = PasswordEmptyException.class)
//    public void testCreate_EmptyPasswordException_1() throws AbstractException {
//        userService.create("LOGIN", "", "newtest@t.com");
//    }
//
//    @Test(expected = PasswordEmptyException.class)
//    public void testCreate_EmptyPasswordException_2() throws AbstractException {
//        userService.create("LOGIN", NULL_STR, "newtest@t.com");
//    }
//
//    @Test
//    public void testFindByEmail() throws AbstractException {
//        for (final UserDTO user : users) {
//            final UserDTO actualUser = userService.findOneByEmail(user.getEmail());
//            assertEquals(user, actualUser);
//        }
//    }
//
//    @Test(expected = EmailEmptyException.class)
//    public void testFindByEmail_EmailEmptyException_1() throws AbstractException {
//        userService.findOneByEmail("");
//    }
//
//    @Test(expected = EmailEmptyException.class)
//    public void testFindByEmail_EmailEmptyException_2() throws AbstractException {
//        userService.findOneByEmail(NULL_STR);
//    }
//
//    @Test(expected = UserNotFoundException.class)
//    public void testFindByEmail_UserNotFoundException() throws AbstractException {
//        userService.findOneByEmail(NONE_STR);
//    }
//
//    @Test
//    public void testFindByLogin() throws AbstractException {
//        for (final UserDTO user : users) {
//            final UserDTO actualUser = userService.findOneByLogin(user.getLogin());
//            assertEquals(user, actualUser);
//        }
//    }
//
//    @Test(expected = LoginEmptyException.class)
//    public void testFindByLogin_LoginEmptyException_1() throws AbstractException {
//        userService.findOneByLogin("");
//    }
//
//    @Test(expected = LoginEmptyException.class)
//    public void testFindByLogin_LoginEmptyException_2() throws AbstractException {
//        userService.findOneByLogin(NULL_STR);
//    }
//
//    @Test(expected = UserNotFoundException.class)
//    public void testFindByLogin_UserNotFoundException() throws AbstractException {
//        userService.findOneByLogin(NONE_STR);
//    }
//
//    @Test
//    public void testIsEmailExist() throws AbstractException {
//        for (final UserDTO user : users) {
//            assertTrue(userService.isEmailExist(user.getEmail()));
//            assertFalse(userService.isEmailExist(user.getEmail() + user.getEmail()));
//        }
//    }
//
//    @Test(expected = EmailEmptyException.class)
//    public void testIsEmailExists_LoginEmptyException_1() throws AbstractException {
//        userService.isEmailExist("");
//    }
//
//    @Test(expected = EmailEmptyException.class)
//    public void testIsEmailExists_LoginEmptyException_2() throws AbstractException {
//        userService.isEmailExist(NULL_STR);
//    }
//
//
//    @Test
//    public void testIsLoginExist() throws AbstractException {
//        for (final UserDTO user : users) {
//            assertTrue(userService.isLoginExist(user.getLogin()));
//            assertFalse(userService.isLoginExist(user.getLogin() + user.getLogin()));
//        }
//    }
//
//    @Test(expected = LoginEmptyException.class)
//    public void testIsLoginExists_LoginEmptyException_1() throws AbstractException {
//        userService.isLoginExist("");
//    }
//
//    @Test(expected = LoginEmptyException.class)
//    public void testIsLoginExists_LoginEmptyException_2() throws AbstractException {
//        userService.isLoginExist(NULL_STR);
//    }
//
//    @Test
//    public void testLockUserByLogin() throws AbstractException {
//        for (final UserDTO user : users) {
//            user.setLocked(false);
//            assertFalse(user.isLocked());
//            userService.lockUserByLogin(user.getLogin());
//            assertTrue(userService.findOneById(user.getId()).isLocked());
//        }
//    }
//
//    @Test(expected = LoginEmptyException.class)
//    public void testLockUserByLogin_LoginEmptyException_1() throws AbstractException {
//        userService.lockUserByLogin("");
//    }
//
//    @Test(expected = LoginEmptyException.class)
//    public void testLockUserByLogin_LoginEmptyException_2() throws AbstractException {
//        userService.lockUserByLogin(NULL_STR);
//    }
//
//    @Test
//    public void testRemoveByEmail() throws AbstractException {
//        for (final UserDTO user : users) {
//            assertTrue(userService.existById(user.getId()));
//            assertNotEquals(0, projectService.findAll(user.getId()).size());
//            assertNotEquals(0, taskService.findAll(user.getId()).size());
//            taskService.removeAll(user.getId());
//            projectService.removeAll(user.getId());
//            userService.removeByEmail(user.getEmail());
//            assertFalse(userService.existById(user.getId()));
//            assertEquals(0, projectService.findAll(user.getId()).size());
//            assertEquals(0, taskService.findAll(user.getId()).size());
//        }
//    }
//
//    @Test(expected = ModelNotFoundException.class)
//    public void testRemoveOne() throws AbstractException {
//        userService.removeOne(null);
//    }
//
//    @Test
//    public void testRemoveByLogin() throws AbstractException {
//        for (final UserDTO user : users) {
//            assertTrue(userService.existById(user.getId()));
//            assertNotEquals(0, projectService.findAll(user.getId()).size());
//            assertNotEquals(0, taskService.findAll(user.getId()).size());
//            taskService.removeAll(user.getId());
//            projectService.removeAll(user.getId());
//            userService.removeByLogin(user.getLogin());
//            assertFalse(userService.existById(user.getId()));
//            assertEquals(0, projectService.findAll(user.getId()).size());
//            assertEquals(0, taskService.findAll(user.getId()).size());
//        }
//    }
//
//    @Test
//    public void testSetPassword() throws AbstractException {
//        for (final UserDTO user : users) {
//            final String oldHash = user.getPasswordHash();
//            userService.setPassword(user.getId(), "NEW_PASS" + user.getLogin());
//            assertNotEquals(oldHash, userService.findOneById(user.getId()).getPasswordHash());
//        }
//    }
//
//    @Test(expected = PasswordEmptyException.class)
//    public void testSetPassword_PasswordEmptyException_1() throws AbstractException {
//        userService.setPassword(users.get(0).getId(), "");
//    }
//
//
//    @Test(expected = PasswordEmptyException.class)
//    public void testSetPassword_PasswordEmptyException_2() throws AbstractException {
//        userService.setPassword(users.get(0).getId(), NULL_STR);
//    }
//
//    @Test
//    public void testUnlockUserByLogin() throws AbstractException {
//        for (final UserDTO user : users) {
//            user.setLocked(true);
//            assertTrue(user.isLocked());
//            userService.unlockUserByLogin(user.getLogin());
//            assertFalse(userService.findOneById(user.getId()).isLocked());
//        }
//    }
//
//    @Test(expected = LoginEmptyException.class)
//    public void testUnlockUserByLogin_LoginEmptyException_1() throws AbstractException {
//        userService.unlockUserByLogin("");
//    }
//
//    @Test(expected = LoginEmptyException.class)
//    public void testUnlockUserByLogin_LoginEmptyException_2() throws AbstractException {
//        userService.unlockUserByLogin(NULL_STR);
//    }
//
//    @Test(expected = LoginEmptyException.class)
//    public void testLoginEmptyUnlockUserByLogin() throws AbstractException {
//        userService.unlockUserByLogin("");
//    }
//
//    @Test
//    public void testUserUpdateById() throws AbstractException {
//        for (final UserDTO user : users) {
//            final String firstName = user.getFirstName() + "TEST";
//            final String lastName = user.getLastName() + "TEST";
//            final String middleName = user.getMiddleName() + "TEST";
//            assertNotEquals(user.getFirstName(), firstName);
//            assertNotEquals(user.getLastName(), lastName);
//            assertNotEquals(user.getMiddleName(), middleName);
//            userService.updateUserById(user.getId(), firstName, lastName, middleName);
//            @NotNull final UserDTO actualUser = userService.findOneById(user.getId());
//            assertEquals(firstName, actualUser.getFirstName());
//            assertEquals(lastName, actualUser.getLastName());
//            assertEquals(middleName, actualUser.getMiddleName());
//        }
//    }
//
//    @Test(expected = IdEmptyException.class)
//    public void testUserUpdate_IdEmptyException_1() throws AbstractException {
//        userService.updateUserById("", "firstName", "lastName", "middleName");
//    }
//
//    @Test(expected = IdEmptyException.class)
//    public void testUserUpdate_IdEmptyException_2() throws AbstractException {
//        userService.updateUserById(NULL_STR, "firstName", "lastName", "middleName");
//    }
//
//}
