package ru.t1.amsmirnov.taskmanager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.task.TaskCreateRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.task.TaskCreateResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-create";

    @NotNull
    public static final String DESCRIPTION = "Create new task.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskCreateListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME:");
        @NotNull final String taskName = TerminalUtil.SCANNER.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        @NotNull final String taskDescription = TerminalUtil.SCANNER.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken(), taskName, taskDescription);
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
