package ru.t1.amsmirnov.taskmanager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.task.TaskUnbindFromProjectResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

@Component
public final class TaskUnbindFromProjectListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-unbind-from-project";

    @NotNull
    public static final String DESCRIPTION = "Bind task to project";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskUnbindFromProjectListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken(), taskId,projectId);
        @NotNull final TaskUnbindFromProjectResponse response = taskEndpoint.unbindTaskFromProject(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
